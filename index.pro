QT += qml
QT += quick
QT += sql

CONFIG += c++17
QMAKE_LINK += -nostdlib++

VERSION_MAJOR = 1
VERSION_MINOR = 0
VERSION_BUILD = 0

VERSION = $${VERSION_MAJOR}.$${VERSION_MINOR}.$${VERSION_BUILD}

DEFINES += INDEX_VERSION_STRING=\\\"$$VERSION\\\"

linux:unix:!android {
} else:android {

    message(Building helpers for Android)
    DEFINES += STATIC_KIRIGAMI

#DEFAULT COMPONENTS DEFINITIONS
    DEFINES *= \
        COMPONENT_EDITOR \
        COMPONENT_FM \
        COMPONENT_TAGGING \
        COMPONENT_SYNCING \
        MAUIKIT_STYLE

    DEFINES -= COMPONENT_STORE
    ANDROID_PACKAGE_SOURCE_DIR = $$PWD/android_files

   DISTFILES += \
$$PWD/android_files/AndroidManifest.xml

    include($$PWD/3rdparty/kirigami/kirigami.pri)
    include($$PWD/3rdparty/mauikit/mauikit.pri)

} else {
    message("Unknown configuration")
}

# The following define makes your compiler emit warnings if you use
# any feature of Qt which as been marked deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
    $$PWD/src/main.cpp \
    $$PWD/src/index.cpp \

HEADERS += \
    $$PWD/src/index.h \
    $$PWD/src/inx.h \

RESOURCES += \
    $$PWD/src/qml.qrc \
    $$PWD/src/index_assets.qrc

# Additional import path used to resolve QML modules in Qt Creator's code model
QML_IMPORT_PATH =

# Additional import path used to resolve QML modules just for Qt Quick Designer
QML_DESIGNER_IMPORT_PATH =

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

include($$PWD/install.pri)

